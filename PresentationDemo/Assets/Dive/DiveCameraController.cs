﻿using UnityEngine;
using System.Collections;

public class DiveCameraController : MonoBehaviour {

    private float _previousNorth;

    bool _isDown;

	// Use this for initialization
	void Start () {
        Input.compass.enabled = true;
        _previousNorth = Input.compass.magneticHeading;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (Mathf.Abs(Input.compass.magneticHeading - _previousNorth) > 5)
        {
            _isDown = true;
            _previousNorth = Input.compass.magneticHeading;
        }
        else
        {
            _isDown = false;
        }
	}

    void OnGUI(){
        GUI.skin.label.fontSize = 30;
        GUILayout.BeginArea(new Rect(10, 10, 300, 300));
        GUILayout.Label(Input.compass.magneticHeading.ToString());
        GUILayout.Label(_isDown.ToString());
        var diff = Mathf.Abs(Input.compass.magneticHeading - _previousNorth);

        GUILayout.Label(diff.ToString());
        GUILayout.EndArea();
    }
}
