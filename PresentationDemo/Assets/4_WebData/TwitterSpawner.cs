﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Net;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


public class TwitterSpawner : MonoBehaviour {


    string _encodedAuth = ""; //encoded twitter auth goes here

    public GameObject FollowerEnemyTemplate;

    string _bearerToken;
    List<string> _ids;

	// Use this for initialization
	IEnumerator Start () {

        yield return StartCoroutine(GetBearerTokenWWW());
        yield return StartCoroutine(LoadFollowerIds());
        yield return StartCoroutine(LoadFollowerDetails());
        

	}

    IEnumerator SpawnFollowers(JArray followers)
    {
        
        foreach (var follower in followers)
        {
            
            var location = UnityEngine.Random.insideUnitSphere * 50;
            location.y = 1;
            var followerObject = JObject.Parse(follower.ToString());
            var enemy = Instantiate(FollowerEnemyTemplate, location, Quaternion.identity) as GameObject;
            enemy.name = followerObject["screen_name"].ToString();


            var followersCount = followerObject["followers_count"].ToObject<float>();

            float minScale = 1;
            float maxScale = 5;
            var scale = Mathf.Clamp(followersCount * .1f, minScale, maxScale); ;

            enemy.transform.localScale = Vector3.one * scale;
            
            //twitter adds suffixes to their image urls (normal, bigger, etc) 
            var imageUrl = followerObject["profile_image_url"]
                .ToString()
                .Replace("\"", "")
                .Replace("normal", "bigger");

            StartCoroutine(ApplyImageTexture(enemy, imageUrl));
            yield return new WaitForSeconds(.1f);
        }
    }

    private IEnumerator ApplyImageTexture(GameObject obj, string imageUrl)
    {
        var request = new WWW(imageUrl);
        
        yield return request;
        
        obj.GetComponent<MeshRenderer>().material.mainTexture = request.texture;
        
    }
    
    private IEnumerator GetBearerTokenWWW()
    {
        var url = "https://api.twitter.com/oauth2/token";
        string body = "grant_type=client_credentials";
        var bodyBytes = Encoding.ASCII.GetBytes(body.ToCharArray());

        var headers = new Dictionary<string, string>();
        headers.Add("Authorization", "Basic " +_encodedAuth);
 
        var form = new WWWForm();
        form.AddField("grant_type", "client_credentials");

        var request = new WWW(url, form.data, headers);

        yield return request;

        var responseObject = JObject.Parse(request.text);

        _bearerToken = responseObject["access_token"].ToString();

       
    }

    private IEnumerator LoadFollowerIds()
    {
        
        var url = "https://api.twitter.com/1.1/followers/ids.json?screen_name=briercan";
        var headers = new Dictionary<string, string>();
        headers.Add("Authorization", "Bearer " + _bearerToken);
        var request = new WWW(url,null, headers);

        yield return request;
        var responseObject = JObject.Parse(request.text);
        var ids = responseObject["ids"].ToArray();

        _ids = new List<string>();
        foreach (var id in ids)
        {
            var strippedId = id.ToString().Replace("\"", "");
            _ids.Add(strippedId.ToString());
        }
        
    }

    private IEnumerator LoadFollowerDetails()
    {
        var urlFormat = "https://api.twitter.com/1.1/users/lookup.json?user_id={0}";

        var idList = string.Join(",", _ids.ToArray());
        var url = string.Format(urlFormat, WWW.EscapeURL(idList));

        var headers = new Dictionary<string, string>();
        headers.Add("Authorization", "Bearer " + _bearerToken);
        var request = new WWW(url, null, headers);
        yield return request;

        var responseObject = JArray.Parse(request.text);
        StartCoroutine(SpawnFollowers(responseObject));
        
        
    }

	// Update is called once per frame
	void Update () {
	
	}
}
