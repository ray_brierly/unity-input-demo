﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WebcamControl : MonoBehaviour
{

    private WebCamTexture _webcamTexture;


    public Color DominantColor;

    // Use this for initialization
    private void Start()
    {
        _webcamTexture = new WebCamTexture();

        var webcamObjects = GameObject.FindGameObjectsWithTag("WebcamObject");
        foreach (var obj in webcamObjects)
        {
            obj.renderer.materials[0].mainTexture = _webcamTexture;
        }


        _webcamTexture.Play();

        InvokeRepeating("ProcessWebcamPixels", 0, 2);
    }

    // Update is called once per frame
    private void Update()
    {

    }

    private void ProcessWebcamPixels()
    {
        if (_webcamTexture == null)
            return;

        var histogram = new Dictionary<Color, int>();

        var pixels = _webcamTexture.GetPixels();
        //there are a lot of pixels and we don't have time to read them all, so we skip over a bunch of them.
        var skip = 200;
        for (int i = 0; i < pixels.Length; i += skip)
        {
            var c = pixels[i];
            if (c.grayscale < .8f && c.grayscale > .1f)
            {
                if (!histogram.ContainsKey(c))
                {
                    histogram[c] = 1;
                }
                else
                {
                    histogram[c]++;
                }
            }
        }

        var colorList = histogram.ToList();
        colorList.Sort(delegate(KeyValuePair<Color, int> pair1, KeyValuePair<Color, int> pair2)
        {
            return pair2.Value.CompareTo(pair1.Value);
        });

        //get the top 10 colors
        var topColors = colorList
            .Select(x => x.Key);

        if (topColors.Any())
        {
            DominantColor = topColors.First();
        }
        

    }

}
