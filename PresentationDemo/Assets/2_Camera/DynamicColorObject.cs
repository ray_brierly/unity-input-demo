﻿using UnityEngine;
using System.Collections;

public class DynamicColorObject : MonoBehaviour {


    public float DynamicObjectSaturation = .5f;
    public float DynamicObjectBrightness = .5f;

    private WebcamControl _webcamControl;

	// Use this for initialization
	void Start ()
	{
	    _webcamControl = GameObject.FindGameObjectWithTag("WebcamControl")
            .GetComponent<WebcamControl>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var mainColor = _webcamControl.DominantColor;
        if (gameObject.renderer.isVisible)
        {
            foreach (var m in gameObject.renderer.materials)
            {
                if (m.name.Contains("Color1"))
                {
                    var c = ColorUtils.SetValues(mainColor, DynamicObjectSaturation, DynamicObjectBrightness);
                    m.SetColor("_Color", c);
                }

                if (m.name.Contains("Color2"))
                {
                    var c = ColorUtils.GetShiftedColor(mainColor, .33f);
                    c = ColorUtils.SetValues(c, DynamicObjectSaturation, DynamicObjectBrightness);
                    m.SetColor("_Color", c);
                    //m.color = _webCamControl.Color2;
                }

                if (m.name.Contains("Color3"))
                {
                    var c = ColorUtils.GetShiftedColor(mainColor, .66f);
                    c = ColorUtils.SetValues(c, DynamicObjectSaturation, DynamicObjectBrightness);
                    m.SetColor("_Color", c);
                    //m.color = _webCamControl.Color2;
                }
            }
        }
	}
}
