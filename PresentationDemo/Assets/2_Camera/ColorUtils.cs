﻿using UnityEngine;
using System.Collections;

public class ColorUtils {

	public static Color GetComplimentaryColor(Color rgb) {
		return GetShiftedColor (rgb, .5f);
	}

	public static Color GetShiftedColor(Color rgb, float shift){
		var hsbColor = new HSBColor (rgb);
		//Debug.Log (hsbColor);
		var shiftedHue = HueShift (hsbColor.h, shift);
		hsbColor.h = shiftedHue;
		//Debug.Log (hsbColor);
		return hsbColor.ToColor ();
	}

	public static Color SetValues(Color color, float? saturation, float? brightness){
		var c = color;
		if (saturation.HasValue) {
			c = SetSaturation(c, saturation.Value);
		}
		if (brightness.HasValue) {
			c = SetBrightness(c, brightness.Value);
		}
		return c;
	}

	public static Color SetSaturation(Color color, float saturation){

		var hsb = new HSBColor (color);
		hsb.s = saturation;
		return hsb.ToColor ();
	}

	public static Color SetBrightness(Color color, float brightness){
		
		var hsb = new HSBColor (color);
		hsb.b = brightness;
		return hsb.ToColor ();
	}


	static float HueShift(float h, float shiftAmount) 
	{ 
		h+=shiftAmount; 
		while (h>=360.0f) h-=360.0f; 
		while (h<0.0f) h+=360.0f; 
		return h; 
	}

}
