﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class ListenForAudio : MonoBehaviour {

   
    public Light TestLight;

    public float Sensitivity = 100;

    private const int FREQUENCY = 44100;
    private const int CLIP_LENGTH = 10;

	// Use this for initialization
	void Start () {

        audio.clip = Microphone.Start(null, true, CLIP_LENGTH, FREQUENCY);
        audio.loop = true;
        audio.mute = true;
        while (!(Microphone.GetPosition(null) > 0)) { }
        audio.Play();
	}
	
	// Update is called once per frame
	void Update () {

        var vol = GetAveragedVolume() * Sensitivity;
        
        TestLight.intensity = vol;
	}

    float GetAveragedVolume()
    {
        float[] data = new float[256];
        float a = 0;
        audio.GetOutputData(data, 0);
        foreach (float s in data)
        {
            a += Mathf.Abs(s);
        }
        return a / 256;
    }
}
