﻿using UnityEngine;
using System.Collections;

public class PhoneMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Input.compass.enabled = true;
        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
        
        Quaternion deviceRotation;
        if (SystemInfo.supportsGyroscope && Input.gyro.enabled)
        {
            
            deviceRotation = Input.gyro.attitude;
        }
        else
        {
            var dir = Vector3.zero;
 
            // we assume that the device is held parallel to the ground
            // and the Home button is in the right hand
 
            // remap the device acceleration axis to game coordinates:
            //  1) XY plane of the device is mapped onto XZ plane
            //  2) rotated 90 degrees around Y axis
            dir.x = -Input.acceleration.y;
            dir.z = Input.acceleration.x;
            dir.y = Input.compass.trueHeading;
            deviceRotation = Quaternion.Euler(dir * 90);
        }
        
        transform.rotation = deviceRotation;
        
	}
}
